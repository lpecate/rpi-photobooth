# Photo Booth

This script is based on *Raspberry PI powered Photo Booth* by *ericBcreator* <ericBcreator@gmail.com>

> **Raspberry PI powered Photo Booth**
>
> Last updated 20170902 by ericBcreator
>
> This code is free for personal use, not for commercial purposes.
> Please leave this header intact.
>
> contact: ericBcreator@gmail.com
>
> original by Jack Barker: http://jackbarker.com.au/photo-booth/

The script works as follow:
1. Script initializes for a given time: 60 seconds by default.
2. Script displays a blinking screen telling to "Press the button!"
   - It will triggers photo captures: 3 photos by default
   - Then it will playbacks captured photos.
   - Go back to step 2.

**My contribution:**

I added this the capabilities to run under newer Raspberry Pi OS distributions as Python and some dependencies has evolved. I use Tkinter for that purpose.
I also added some features like kinbindings as ESCAPE to exit the script.

## Getting started

### Hardware

- Connect a push button to GPIO 21 (pin 40) and GND (pin 39) (last row from the 40 GPIO pins, closest to USB and Ethernet ports): it will be used to trigger photo capture.
- Connect arcade LED to GPIO 13 (pin 33) and GND (pin 34) (depending the LED, you should have to add a resistor to reduce the current inside it).
- Connect power LED to pin 2 or 4 (5V pin) and to a GND pin (i.e. pin 14): it will be turned as long the Rapsberry Pi is powered.
- Connect a push button to GPIO 5 (pin 29) and GND (pin 30) for soft shutdown:
  - You will have to add a new line inside `config.txt` file from boot partition: `dtoverlay=gpio-shutdown,gpio_pin=5`
- Optional: DS3231 RTC module
  - Connect module VCC pin to pin 1 (3.3V)
  - Connect module GND pin to GND pin
  - Connect module SDA pin to pin 3 (SDA1/GPIO2)
  - Connect module SCL pin to pin 5 (SCL1/GPIO3)

### Software/Dependencies

- Install ImageMagick if not already done: `sudo apt-get install -y imagemagick`
- Install dependencies `sudo apt-get install -y python3-pil python3-pil.imagetk`
- Enable 'Legacy Camera' interface using `sudo raspi-config`. This will be necessary until [Picamera2](https://github.com/raspberrypi/picamera2) library will be in a stable state, then a port for this script would be done.
- Disable screen blanking from Raspberry Pi configuration tool (GUI > Display > Screen Blanking or `raspi-config`).
- To avoid the mouse stays on top of the Photobooth, install unclutter: `sudo apt-get install unclutter`
- Optional: DS3231 RTC module instructions (for systemd Linux distributions only – Source: https://learn.adafruit.com/adding-a-real-time-clock-to-raspberry-pi/set-rtc-time)
  - Install software dependencies: `sudo apt-get install python3-smbus i2c-tools`
  - Check DS3231 module ID from I2C bus address: `sudo i2cdetect -y 1`
  - Add to the end of `/root/config.txt` file: `dtoverlay=i2c-rtc,ds3231`
  - Boot the Raspberry Pi and check I2C ID using `sudo i2cdetect -y 1`. It should display `UU` at the previous module address.
  - Disable 'fake hwclock':
    - `sudo apt-get -y remove fake-hwclock`
    - `sudo update-rc.d -f fake-hwclock remove`
    - `sudo systemctl disable fake-hwclock`
  - Start the original hardware clock by editing `/lib/udev/hwclock-set` file. Comment these three lines:
    ```
	#if [ -e /run/systemd/system ] ; then
    # exit 0
    #fi
	```
	And also these two lines: `/sbin/hwclock --rtc=$dev --systz --badyear` and `/sbin/hwclock --rtc=$dev --systz`
  - Check the time from RTC module: `sudo hwclock -r` (It should be wrong if you never used the module yet.)
  - Connect to Internet (Ethernet or Wi-Fi) to automatically set the time using the NTP service.
  - Check the time from RTC module again: `sudo hwclock -r` (It should be be right that time.)

### Run the script

Go to the folder where you get this script into. Run `python ./Photobooth.py`

### Autostart

To make the script run automatically after booting up, edit the `rc.local` file:
```
sudo nano /etc/rc.local
```

At the end (but before the exit command) add this line:
```
sudo python /home/pi/photobooth/Photobooth.py &
```

Don't forget the `&` sign or else the Pi won't boot up but wait until the script is finished (which - normally - won't happen).

**Workaround:** If this does not work, you can do: `sudo cp scripts/autostart-photobooth.desktop /etc/xdg/autostart/autostart-photobooth.desktop`
